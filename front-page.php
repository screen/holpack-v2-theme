<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'hlp_hero_bg_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <?php /* HOME: HERO SECTION */ ?>
        <section class="home__hero col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background-image: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row align-items-center">
                    <div class="home__hero--image col-xl-5 col-lg-5 col-md-4 col-sm-12 col-12 align-self-end d-xl-block d-lg-block d-md-block d-sm-none d-none">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'hlp_hero_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'large', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" data-aos="fade" data-aos-delay="50" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                    </div>
                    <div class="home__hero--content col-xl-7 col-lg-7 col-md-8 col-sm-12 col-12">
                       <div class="home__hero--title" data-aos="fade" data-aos-delay="50">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hlp_hero_title', true)); ?>
                        </div>
                        <?php $group_desc = get_post_meta(get_the_ID(), 'hlp_hero_items_group', true); ?>
                        <?php if (!empty($group_desc)) { ?>
                        <div class="content-desc row" data-aos="fade" data-aos-delay="50">
                            <?php foreach ($group_desc as $item) { ?>
                            <div class="hero-item col-6">
                                <?php echo $item['description']; ?>
                            </div>
                            <?php } ?>
                        </div>
                        <?php } ?>

                        <h3 data-aos="fade" data-aos-delay="50"><?php echo get_post_meta(get_the_ID(), 'hlp_hero_subtitle', true); ?></h3>

                        <a href="<?php echo get_post_meta(get_the_ID(), 'hlp_hero_button_link', true); ?>" class="btn btn-lg btn-hero" title="<?php echo get_post_meta(get_the_ID(), 'hlp_hero_button_text', true); ?>" data-aos="fade" data-aos-delay="300"><?php echo get_post_meta(get_the_ID(), 'hlp_hero_button_text', true); ?></a>
                    </div>
                </div>
            </div>
        </section>
        <?php /* HOME: MEDIA SECTION */ ?>
        <section class="home__media col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="home__media--image col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'hlp_media_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'medium', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                    </div>
                    <div class="home__media--content col-xl-9 col-lg-9 col-md-9 col-sm-9 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'hlp_media_title', true); ?></h2>
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hlp_media_desc', true)); ?>
                    </div>
                </div>
            </div>
        </section>
        <?php /* HOME: SOLUTIONS SECTION */ ?>
        <section class="home__solutions col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="home__solutions--title  col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hlp_solutions_title', true)); ?>
                    </div>
                </div>
                <?php $group_desc = get_post_meta(get_the_ID(), 'hlp_solutions_items_group', true); ?>
                <?php if (!empty($group_desc)) { ?>
                <div class="row no-gutters row-solutions align-items-start">
                    <?php $i = 1; ?>
                    <?php foreach ($group_desc as $item) { ?>
                    <?php $delay = 150 * $i; ?>
                    <div class="hero-item col-xl col-lg col-md-6 col-sm-6 col-12">
                        <div class="hero-item-wrapper" style="background: url(<?php echo $item['item_bg']; ?>);" data-aos="fade" data-aos-delay="<?php echo $delay; ?>">
                            <h2><?php echo $item['title']; ?></h2>
                            <div class="hero-item-description">
                                <?php echo apply_filters('the_content', $item['description']); ?>
                            </div>
                            <div class="hidden-title">
                                <h3><?php echo $item['title']; ?></h3>
                            </div>
                            <div class="hero-see-more">
                                <div class="icon"><span></span><span></span></div>
                                <div class="show"><?php _e('See More', 'holpack'); ?></div>
                                <div class="hidden"><?php _e('See Less', 'holpack'); ?></div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; } ?>
                </div>
                <?php } ?>
            </div>
        </section>
        <?php /* HOME: SOLUTIONS SECTION */ ?>
        <section class="home__contact col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters align-items-center">
                    <div class="home__contact-image col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-xl-block d-lg-block d-md-block d-sm-none d-none">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'hlp_contact_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" data-aos="fade" data-aos-delay="150" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                    </div>
                    <div class="home__contact-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" data-aos="fade" data-aos-delay="200">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hlp_contact_desc', true)); ?>
                        <?php echo get_template_part('templates/templates-contact-form'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
