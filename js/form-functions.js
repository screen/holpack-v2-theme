var submitBtn = document.getElementById('submitBtn'),
    modalSubmitBtn = document.getElementById('modalSubmitBtn'),
    grecaptchaWidget = '',
    grecaptchaContact = '',
    passd = true;



function recaptchaModalCallback() {
    var currentError = document.getElementById('recaptchaModal').nextElementSibling;
    currentError.classList.add('d-none');
}

function recaptchaContactCallback() {
    var currentError = document.getElementById('recaptchaContact').nextElementSibling;
    currentError.classList.add('d-none');
}

const countryList = ["Afghanistan", "Åland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas (the)", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia (Plurinational State of)", "Bonaire, Sint Eustatius and Saba", "Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory (the)", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cabo Verde", "Cambodia", "Cameroon", "Canada", "Cayman Islands (the)", "Central African Republic (the)", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands (the)", "Colombia", "Comoros (the)", "Congo (the Democratic Republic of the)", "Congo (the)", "Cook Islands (the)", "Costa Rica", "Croatia", "Cuba", "Curaçao", "Cyprus", "Czechia", "Côte d'Ivoire", "Denmark", "Djibouti", "Dominica", "Dominican Republic (the)", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Eswatini", "Ethiopia", "Falkland Islands (the) [Malvinas]", "Faroe Islands (the)", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories (the)", "Gabon", "Gambia (the)", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (the)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea (the Democratic People's Republic of)", "Korea (the Republic of)", "Kuwait", "Kyrgyzstan", "Lao People's Democratic Republic (the)", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands (the)", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia (Federated States of)", "Moldova (the Republic of)", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands (the)", "New Caledonia", "New Zealand", "Nicaragua", "Niger (the)", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands (the)", "Norway", "Oman", "Pakistan", "Palau", "Palestine, State of", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines (the)", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Republic of North Macedonia", "Romania", "Russian Federation (the)", "Rwanda", "Réunion", "Saint Barthélemy", "Saint Helena, Ascension and Tristan da Cunha", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin (French part)", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten (Dutch part)", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "South Sudan", "Spain", "Sri Lanka", "Sudan (the)", "Suriname", "Svalbard and Jan Mayen", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan (Province of China)", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands (the)", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates (the)", "United Kingdom of Great Britain and Northern Ireland (the)", "United States Minor Outlying Islands (the)", "United States of America (the)", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela (Bolivarian Republic of)", "Viet Nam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"];


function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function validateZipCode(elementValue) {
    var zipCodePattern = /^\d{5}$|^\d{5}-\d{4}$/;
    return zipCodePattern.test(elementValue);
}

function telephoneCheck(str) {
    var patt = new RegExp(/^(\+{0,})(\d{0,})([(]{1}\d{1,3}[)]{0,}){0,}(\s?\d+|\+\d{2,3}\s{1}\d+|\d+){1}[\s|-]?\d+([\s|-]?\d+){1,2}(\s){0,}$/gm);
    return patt.test(str);
}

if (submitBtn != null) {
    submitBtn.addEventListener('click', function(e) {
        var currentError = document.getElementById('recaptchaContact').nextElementSibling;
        e.preventDefault();
        passd = true;

        /* NAME VALIDATION  */
        var inputText = document.getElementById('fullname_contact').value;
        var elements = document.getElementById('fullname_contact').nextElementSibling;
        if (inputText == '' || inputText == null) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_nombre;
            passd = false;
        } else {
            if (inputText.length < 3) {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = custom_admin_url.invalid_nombre;
                passd = false;
            } else {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = '';
            }
        }

        /* EMAIL VALIDATION  */
        var inputText = document.getElementById('email_contact').value;
        var elements = document.getElementById('email_contact').nextElementSibling;
        if (inputText == '' || inputText == null) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_email;
            passd = false;
        } else {
            if (validateEmail(inputText) == false) {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = custom_admin_url.invalid_email;
                passd = false;
            } else {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = '';
            }
        }

        /* PHONE VALIDATION  */
        var inputText = document.getElementById('phone_contact').value;
        var elements = document.getElementById('phone_contact').nextElementSibling;
        if (inputText == '' || inputText == null) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_phone;
            passd = false;
        } else {
            if (telephoneCheck(inputText) == false) {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = custom_admin_url.invalid_phone;
                passd = false;
            } else {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = '';
            }
        }

        /* COMPANY VALIDATION  */
        var inputText = document.getElementById('company_contact').value;
        var elements = document.getElementById('company_contact').nextElementSibling;
        if (inputText == '' || inputText == null) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_company;
            passd = false;
        } else {

            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = '';
        }

        /* ZIPCODE VALIDATION  */
        var inputText = document.getElementById('zipcode_contact').value;
        var elements = document.getElementById('zipcode_contact').nextElementSibling;
        if (inputText == '' || inputText == null) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_zipcode;
            passd = false;
        } else {
            if (validateZipCode(inputText) == false) {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = custom_admin_url.invalid_zipcode;
                passd = false;
            } else {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = '';
            }
        }


        /* COUNTRY VALIDATION  */
        var inputText = document.getElementById('country_contact').value;
        var elements = document.getElementById('country_contact').nextElementSibling;
        if (inputText == '' || inputText < 0) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_country;
            passd = false;
        } else {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = '';
        }

        /* MESSAGE VALIDATION  */
        var inputText = document.getElementById('message_contact').value;
        var elements = document.getElementById('message_contact').nextElementSibling;
        if (inputText == '' || inputText < 0) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_message;
            passd = false;
        } else {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = '';
        }

        if (grecaptcha && grecaptcha.getResponse(grecaptchaContact).length > 0) {
            currentError.classList.add('d-none');
        } else {
            passd = false;
            currentError.classList.remove('d-none');
        }

        if (passd == true) {
            var info = 'action=send_message&fullname_contact=' +
                document.getElementById('fullname_contact').value + '&email_contact=' + document.getElementById('email_contact').value + '&phone_contact=' + document.getElementById('phone_contact').value + '&company_contact=' + document.getElementById('company_contact').value + '&zipcode_contact=' + document.getElementById('zipcode_contact').value + '&country_contact=' + document.getElementById('country_contact').value + '&message_contact=' + document.getElementById('message_contact').value + '&g-recaptcha-response=' + grecaptcha.getResponse(grecaptchaContact);
            var elements = document.getElementsByClassName('loader-css');
            elements[0].classList.toggle("d-none");
            /* SEND AJAX */
            newRequest = new XMLHttpRequest();
            newRequest.open('POST', custom_admin_url.ajax_url, true);
            newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            newRequest.onload = function() {
                var result = JSON.parse(newRequest.responseText);
                if (result.success == true) {
                    elements[0].classList.toggle("d-none");
                    swal({
                        title: 'Message Sent',
                        text: custom_admin_url.success_form,
                        icon: 'success',
                        buttons: {
                            confirm: {
                                className: 'btn btn-success',
                            },
                        },
                    });
                } else {
                    elements[0].classList.toggle("d-none");
                    swal({
                        title: 'Error on Request',
                        text: custom_admin_url.error_form,
                        icon: 'error',
                        buttons: {
                            confirm: {
                                className: 'btn btn-danger',
                            },
                        },
                    });
                }
            };
            newRequest.send(info);
        }

    });
}

if (modalSubmitBtn != null) {

    modalSubmitBtn.addEventListener('click', function(e) {
        var currentError = document.getElementById('recaptchaModal').nextElementSibling;
        e.preventDefault();
        passd = true;

        /* NAME VALIDATION  */
        var inputText = document.getElementById('modal_fullname_contact').value;
        var elements = document.getElementById('modal_fullname_contact').nextElementSibling;
        if (inputText == '' || inputText == null) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_nombre;
            passd = false;
        } else {
            if (inputText.length < 3) {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = custom_admin_url.invalid_nombre;
                passd = false;
            } else {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = '';
            }
        }

        /* EMAIL VALIDATION  */
        var inputText = document.getElementById('modal_email_contact').value;
        var elements = document.getElementById('modal_email_contact').nextElementSibling;
        if (inputText == '' || inputText == null) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_email;
            passd = false;
        } else {
            if (validateEmail(inputText) == false) {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = custom_admin_url.invalid_email;
                passd = false;
            } else {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = '';
            }
        }

        /* PHONE VALIDATION  */
        var inputText = document.getElementById('modal_phone_contact').value;
        var elements = document.getElementById('modal_phone_contact').nextElementSibling;
        if (inputText == '' || inputText == null) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_phone;
            passd = false;
        } else {
            if (telephoneCheck(inputText) == false) {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = custom_admin_url.invalid_phone;
                passd = false;
            } else {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = '';
            }
        }

        /* COMPANY VALIDATION  */
        var inputText = document.getElementById('modal_company_contact').value;
        var elements = document.getElementById('modal_company_contact').nextElementSibling;
        if (inputText == '' || inputText == null) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_company;
            passd = false;
        } else {

            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = '';
        }

        /* ZIPCODE VALIDATION  */
        var inputText = document.getElementById('modal_zipcode_contact').value;
        var elements = document.getElementById('modal_zipcode_contact').nextElementSibling;
        if (inputText == '' || inputText == null) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_zipcode;
            passd = false;
        } else {
            if (validateZipCode(inputText) == false) {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = custom_admin_url.invalid_zipcode;
                passd = false;
            } else {
                if (elements.classList.contains('d-none')) {
                    elements.classList.toggle("d-none");
                }
                elements.innerHTML = '';
            }
        }


        /* COUNTRY VALIDATION  */
        var inputText = document.getElementById('modal_country_contact').value;
        var elements = document.getElementById('modal_country_contact').nextElementSibling;
        if (inputText == '' || inputText < 0) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_country;
            passd = false;
        } else {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = '';
        }

        /* MESSAGE VALIDATION  */
        var inputText = document.getElementById('modal_message_contact').value;
        var elements = document.getElementById('modal_message_contact').nextElementSibling;
        if (inputText == '' || inputText < 0) {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = custom_admin_url.error_message;
            passd = false;
        } else {
            if (elements.classList.contains('d-none')) {
                elements.classList.toggle("d-none");
            }
            elements.innerHTML = '';
        }

        if (grecaptcha && grecaptcha.getResponse(grecaptchaModal).length > 0) {
            currentError.classList.add('d-none');
        } else {
            passd = false;
            currentError.classList.remove('d-none');
        }

        if (passd == true) {
            var info = 'action=send_modal_message&fullname_contact=' +
                document.getElementById('modal_fullname_contact').value + '&email_contact=' + document.getElementById('modal_email_contact').value + '&phone_contact=' + document.getElementById('modal_phone_contact').value + '&company_contact=' + document.getElementById('modal_company_contact').value + '&zipcode_contact=' + document.getElementById('modal_zipcode_contact').value + '&country_contact=' + document.getElementById('modal_country_contact').value + '&message_contact=' + document.getElementById('modal_message_contact').value + '&g-recaptcha-response=' + grecaptcha.getResponse(grecaptchaModal);
            var elements = document.getElementsByClassName('modal-loader-css');
            elements[0].classList.toggle("d-none");
            /* SEND AJAX */
            newRequest = new XMLHttpRequest();
            newRequest.open('POST', custom_admin_url.ajax_url, true);
            newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            newRequest.onload = function() {
                var result = JSON.parse(newRequest.responseText);
                if (result.success == true) {
                    elements[0].classList.toggle("d-none");
                    swal({
                        title: 'Message Sent',
                        text: custom_admin_url.success_form,
                        icon: 'success',
                        buttons: {
                            confirm: {
                                className: 'btn btn-success',
                            },
                        },
                    });
                } else {
                    elements[0].classList.toggle("d-none");
                    swal({
                        title: 'Error on Request',
                        text: custom_admin_url.error_form,
                        icon: 'error',
                        buttons: {
                            confirm: {
                                className: 'btn btn-danger',
                            },
                        },
                    });
                }
            };
            newRequest.send(info);
        }

    });
}



/* CUSTOM ON LOAD FUNCTIONS */
function formCustomLoad() {
    var countryAutocomplete = new autoComplete({
        selector: '#country_contact',
        minChars: 1,
        source: function(term, suggest) {
            term = term.toLowerCase();
            var suggestions = [];
            for (i = 0; i < countryList.length; i++)
                if (~countryList[i].toLowerCase().indexOf(term)) suggestions.push(countryList[i]);
            suggest(suggestions);
        }
    });


}

document.addEventListener("DOMContentLoaded", formCustomLoad, false);

var CaptchaCallback = function() {
    var recaptchaModal = document.getElementById('recaptchaModal');
    var recaptchaContact = document.getElementById('recaptchaContact');

    if (recaptchaModal) {
        grecaptchaModal = grecaptcha.render('recaptchaModal', {
            'sitekey': custom_admin_url.recaptcha_site_key
        });
    }

    if (recaptchaContact) {
        grecaptchaContact = grecaptcha.render('recaptchaContact', {
            'sitekey': custom_admin_url.recaptcha_site_key
        });
    }
};