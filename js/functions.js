var heroItems = '',
    mySwiper = '',
    sliderSection = '',
    menuBtn = '';

AOS.init({
    // Global settings:
    disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
    startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
    initClassName: 'aos-init', // class applied after initialization
    animatedClassName: 'aos-animate', // class applied on animation
    useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
    disableMutationObserver: false, // disables automatic mutations' detections (advanced)
    debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
    throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)

    offset: 120, // offset (in px) from the original trigger point
    delay: 0, // values from 0 to 3000, with step 50ms
    duration: 400, // values from 0 to 3000, with step 50ms
    easing: 'ease', // default easing for AOS animations
    once: false, // whether animation should happen only once - while scrolling down
    mirror: false, // whether elements should animate out while scrolling past them
    anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation

});

/* HERO CLICK FUNCTIONS */
function openHeroDesc() {
    heroItems = document.querySelectorAll('.hero-item');
    if (this.classList.contains('active')) {
        for (var i = 0; i < heroItems.length; i++) {
            heroItems[i].classList.remove('active');
            heroItems[i].classList.remove('slim');
        }
        this.classList.remove('slim');
        this.classList.remove('active');

    } else {
        for (var i = 0; i < heroItems.length; i++) {
            heroItems[i].classList.remove('active');
            heroItems[i].classList.add('slim');
        }
        this.classList.toggle('slim');
        this.classList.toggle('active');
    }
}

function openMenu() {
    var menuElm = document.getElementById('menuElm');
    this.classList.toggle('opened');
    menuElm.classList.toggle('header-menu-mobile-hidden');
}

/* CUSTOM ON LOAD FUNCTIONS */
function documentCustomLoad() {
    "use strict";
    menuBtn = document.getElementById('menuBtn');
    menuBtn.addEventListener('click', openMenu, false);


    heroItems = document.querySelectorAll('.hero-item');
    if (heroItems != null) {
        heroItems.forEach(function (btn) {
            btn.addEventListener('click', openHeroDesc, false);
        });
    }

    sliderSection = document.querySelector('.swiper-container');
    if (sliderSection != null) {
        mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            speed: 400,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false
            },
            spaceBetween: 30,
            slidesPerView: 1,
            centeredSlides: true,
            loop: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            }
        });
    }


    var hash = window.location.hash;
    if (hash != '') {
        jQuery('#productTab').find('a[href=' + hash + ']').click();
    }
}

document.addEventListener("DOMContentLoaded", documentCustomLoad, false);
