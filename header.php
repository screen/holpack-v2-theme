<!DOCTYPE html>
<html <?php language_attributes() ?>>

<head>
    <?php /* MAIN STUFF */ ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
    <meta name="robots" content="NOODP, INDEX, FOLLOW" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="dns-prefetch" href="//connect.facebook.net" />
    <link rel="dns-prefetch" href="//facebook.com" />
    <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
    <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
    <link rel="dns-prefetch" href="//google-analytics.com" />
    <?php /* FAVICONS */ ?>
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
    <?php /* THEME NAVBAR COLOR */ ?>
    <meta name="msapplication-TileColor" content="#0F75BC" />
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
    <meta name="theme-color" content="#0F75BC" />
    <?php /* AUTHOR INFORMATION */ ?>
    <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
    <meta name="author" content="Holpack" />
    <meta name="copyright" content="https://holpack.com" />
    <meta name="geo.position" content="25.9150357,-80.3625324" />
    <meta name="ICBM" content="25.9150357,-80.3625324" />
    <meta name="geo.region" content="US" />
    <meta name="geo.placename" content="3840 W 104th St Unit 7, Hialeah, FL 33018, United States" />
    <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
    <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
    <?php wp_title('|', false, 'right'); ?>
    <?php wp_head() ?>
    <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
    <?php /* IE COMPATIBILITIES */ ?>
    <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
    <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
    <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
    <!--[if gt IE 8]><!-->
    <html <?php language_attributes(); ?> class="no-js" />
    <!--<![endif]-->
    <!--[if IE]> <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script> <![endif]-->
    <!--[if IE]> <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script> <![endif]-->
    <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
</head>

<body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
    <?php wp_body_open(); ?>
    <?php $header_options = get_option('hlp_header_settings'); ?>
    <header class="container-fluid container__header p-0" role="banner" itemscope itemtype="http://schema.org/WPHeader">
        <div class="row no-gutters">
            <div class="the-header col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="container">
                    <div class="row align-items-center no-gutters">
                        <div class="header__logo col-xl col-lg col-md-5 col-sm-5 col-6">
                            <a class="navbar-brand" href="<?php echo home_url('/');?>" title="<?php echo get_bloginfo('name'); ?>">
                                <?php $custom_logo_id = get_theme_mod( 'custom_logo' ); ?>
                                <?php $image = wp_get_attachment_image_src( $custom_logo_id , 'logo' ); ?>
                                <?php if (!empty($image)) { ?>
                                <img src="<?php echo $image[0];?>" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-logo" />
                                <?php } else { ?>
                                Navbar
                                <?php } ?>
                            </a>
                        </div>
                        <div class="header__menu col-7 d-xl-block d-lg-block d-md-none d-sm-none d-none">
                            <nav class="navbar navbar-expand-md" role="navigation">
                                <?php
                                    wp_nav_menu( array(
                                        'theme_location'  => 'header_menu',
                                        'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                                        'container'       => 'div',
                                        'container_class' => 'collapse navbar-collapse',
                                        'container_id'    => 'bs-example-navbar-collapse-1',
                                        'menu_class'      => 'navbar-nav ml-auto mr-auto',
                                        'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                        'walker'          => new WP_Bootstrap_Navwalker(),
                                    ) );
                                    ?>
                            </nav>
                        </div>
                        <div class="header__button  col-xl col-lg col-md-7 col-sm-7 col-6 d-xl-block d-lg-block d-md-none d-sm-none d-none">
                            <a href="<?php echo $header_options['phone_number']; ?>" class="header__button-group">
                                <div class="button__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="button__content">
                                    <p><?php echo $header_options['header_button_text']; ?></p>
                                    <p><?php echo $header_options['header_phone_text']; ?></p>
                                </div>
                            </a>
                        </div>
                        <div class="header__button--mobile col-xl col-lg col-md-7 col-sm-7 col-6  d-xl-none d-lg-none d-md-flex d-sm-flex d-flex">
                            <a href="<?php echo $header_options['phone_number']; ?>" class="header__button-group">
                                <div class="button__icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="button__content">
                                    <p><?php echo $header_options['header_button_text']; ?></p>
                                    <p><?php echo $header_options['header_phone_text']; ?></p>
                                </div>
                            </a>

                            <div id="menuBtn" class="menu-btn-opener">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="menuElm" class="header-menu-mobile header-menu-mobile-hidden col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'header_menu',
                        'depth' => 2,
                        'container' => 'div'
                    ) );
                    ?>

                <div class="header-menu-mobile-phone">
                    <a href="<?php echo $header_options['phone_number']; ?>" class="header__button-group">
                        <div class="button__icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="button__content">
                            <p><?php echo $header_options['header_button_text']; ?></p>
                            <p><?php echo $header_options['header_phone_text']; ?></p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </header>
