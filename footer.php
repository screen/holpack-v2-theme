<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row no-gutters align-items-start">
                    <?php if (is_active_sidebar('sidebar_footer')) : ?>
                    <div class="footer-item col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                        <ul id="sidebar-footer1" class="footer-sidebar">
                            <?php dynamic_sidebar('sidebar_footer'); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if (is_active_sidebar('sidebar_footer-2')) : ?>
                    <div class="footer-item col-xl-6 col-lg-6 col-md-4 col-sm-12 col-12">
                        <ul id="sidebar-footer2" class="footer-sidebar">
                            <?php dynamic_sidebar('sidebar_footer-2'); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if (is_active_sidebar('sidebar_footer-3')) : ?>
                    <div class="footer-item col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                        <ul id="sidebar-footer3" class="footer-sidebar">
                            <?php dynamic_sidebar('sidebar_footer-3'); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <div class="w-100"></div>
                    <div class="footer-line col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"></div>
                </div>
            </div>
        </div>
        <div class="footer-copy col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row no-gutters align-items-center">
                    <div class="footer-copy-content footer-copy-content-left col-xl col-lg col-md col-sm-12 col-12">
                        <h5><?php _e('Copyright &copy; 2020 Holpack, All rights reserved', 'holpack'); ?></h5>
                    </div>
                    <div class="footer-copy-content footer-copy-content-right col-xl col-lg col-md col-sm-12 col-12">
                        <h5><?php printf(__('DEVELOPED BY <a href="%s">SMG | DIGITAL MARKETING AGENCY</a>', 'maxicon'), 'http://www.screenmediagroup.com'); ?></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php $modal_settings = get_option('hlp_modal_settings'); ?>
<?php if ($modal_settings['modal_activate'] == '1') { ?>
<button class="btn btn-md btn-fixed-modal" data-toggle="modal" data-target="#customModal"><?php echo $modal_settings['button_text']; ?></button>
<div class="modal custom-modal fade" id="customModal" tabindex="-1" aria-labelledby="customModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <div class="container-fluid p-0">
                    <div class="row no-gutters">
                        <div class="modal-image col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <img src="<?php echo $modal_settings['modal_image']; ?>" alt="Image" class="img-fluid d-xl-block d-lg-block d-md-block d-sm-none d-none" />
                            <h2><?php echo $modal_settings['modal_title']; ?></h2>
                        </div>
                        <div class="modal-form col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                            <?php echo apply_filters('the_content', $modal_settings['form_text']); ?>
                            <?php get_template_part('templates/templates-modal-form'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php wp_footer() ?>
</body>

</html>