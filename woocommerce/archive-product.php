<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header">
    <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
    <h1 class="woocommerce-products-header__title page-title"><?php _e( 'Our <strong>Products</strong>', 'holpack'); ?></h1>
    <?php endif; ?>

    <?php
    /**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
    do_action( 'woocommerce_archive_description' );
    ?>
</header>
<div class="tab-content" id="productTabContent">
    <?php $args = array('taxonomy' => 'product_cat', 'hide_empty' => true); ?>
    <?php $array_terms = get_terms($args); ?>
    <?php $i = 1; ?>
    <?php if (!empty($array_terms)) : ?>
    <?php foreach ($array_terms as $item) { ?>
    <?php $class = ($i == 1) ? 'show active' : ''; ?>
    <div class="tab-pane fade <?php echo $class; ?>" id="<?php echo $item->slug; ?>" role="tabpanel" aria-labelledby="<?php echo $item->slug; ?>-tab">
        <div class="woocommerce">
            <?php $argsposts = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'tax_query' => array(array('taxonomy' => 'product_cat', 'terms' => array($item->term_id), 'field' => 'term_id'))); ?>
            <?php $array_posts = new WP_Query($argsposts); ?>

            <?php woocommerce_product_loop_start(); ?>

            <?php while ($array_posts->have_posts()) : $array_posts->the_post(); ?>
            <?php do_action( 'woocommerce_shop_loop' ); ?>
            <?php wc_get_template_part( 'content', 'product' ); ?>
            <?php endwhile; ?>

            <?php woocommerce_product_loop_end(); ?>

            <?php wp_reset_query(); ?>
        </div>
    </div>
    <?php $i++; } ?>
    <?php endif; ?>
</div>
<?php

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

get_footer( 'shop' );
