<?php
/**
* Template Name: Page Template  - Company
*
* @package holpack
* @subpackage holpack-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php get_template_part('templates/templates-banner-container'); ?>
        <section class="company-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="company-main-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php the_content(); ?>
                        <div class="home__media col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="container">
                                <div class="row align-items-center">
                                    <div class="home__media--image col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'hlp_media_image_id', true); ?>
                                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'medium', false); ?>
                                        <img itemprop="logo" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="150" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                    </div>
                                    <div class="home__media--content col-xl-9 col-lg-9 col-md-9 col-sm-9 col-12">
                                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hlp_media_desc', true)); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="company-benefits-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <?php $array_benefits = get_post_meta(get_the_ID(), 'hlp_benefits_items_group', true); ?>
                    <?php if (!empty($array_benefits)) { ?>
                    <?php foreach ($array_benefits as $item) { ?>
                    <article class="company-benefits-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="150">
                        <picture class="company-benefits-image">
                            <?php $bg_banner = wp_get_attachment_image_src($item['item_bg_id'], 'thumbnail', false); ?>
                            <img itemprop="logo" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $item['item_bg_id'], '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($item['item_bg_id'], '_wp_attachment_image_alt', true ); ?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="150" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        </picture>
                        <h3><?php echo $item['title']; ?></h3>
                        <?php echo apply_filters('the_content', $item['description']); ?>
                    </article>
                    <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>
        <section class="company-testimonial-slider-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php $bg_banner_id = get_post_meta(get_the_ID(), 'hlp_testimonial_bg_id', true); ?>
            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'testimonial_bg', false); ?>
            <img itemprop="logo" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="150" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
            <div class="company-testimonial-slider-wrapper">
                <div class="company-testimonial-slider swiper-container">
                    <?php $array_testimonial = get_post_meta(get_the_ID(), 'hlp_testimonials_items_group', true); ?>
                    <?php if (!empty($array_testimonial)) { ?>
                    <div class="swiper-wrapper">

                        <?php foreach ($array_testimonial as $item) { ?>
                        <div class="swiper-slide">
                            <div class="testimonial-item-wrapper">
                                <p><?php echo $item['testimonial']; ?></p>
                                <h3>- <?php echo $item['autor']; ?></h3>
                            </div>
                        </div>
                        <?php } ?>



                    </div>


                    <?php } ?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </section>
        <section class="company-mission-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="company-mission-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hlp_mission_desc', true)); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="company-values-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="company-values-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hlp_values_title', true)); ?>
                    </div>
                    <?php $array_benefits = get_post_meta(get_the_ID(), 'hlp_values_items_group', true); ?>
                    <?php if (!empty($array_benefits)) { ?>
                    <?php foreach ($array_benefits as $item) { ?>
                    <article class="company-values-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" data-aos="fadeIn" data-aos-delay="150">
                        <picture class="company-values-image">
                            <?php $bg_banner = wp_get_attachment_image_src($item['item_bg_id'], 'thumbnail', false); ?>
                            <img itemprop="logo" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $item['item_bg_id'], '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($item['item_bg_id'], '_wp_attachment_image_alt', true ); ?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="150" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        </picture>
                        <h3><?php echo $item['title']; ?></h3>
                        <?php echo apply_filters('the_content', $item['description']); ?>
                    </article>
                    <?php } ?>
                    <?php } ?>

                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
