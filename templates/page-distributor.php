<?php
/**
* Template Name: Page Template  - Distributor
*
* @package holpack
* @subpackage holpack-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php get_template_part('templates/templates-banner-container'); ?>
        <div class="distributor-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="distributor-main-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php the_content(); ?>
                    </div>
                    <div class="distributor-main-form col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hlp_distributor_form_desc', true)); ?>
                        <?php echo get_template_part('templates/templates-contact-form'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
