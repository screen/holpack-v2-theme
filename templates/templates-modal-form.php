<form class="modal-form-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="row">
        <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input type="text" id="modal_fullname_contact" name="fullname" class="form-control" placeholder="<?php _e('FULL NAME', 'holpack'); ?>" />
            <small class="danger custom-danger d-none error-fullname"></small>
        </div>
        <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input type="email" id="modal_email_contact" name="email" class="form-control" placeholder="<?php _e('EMAIL ADDRESS', 'holpack'); ?>" />
            <small class="danger custom-danger d-none error-email"></small>
        </div>
        <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input type="phone" id="modal_phone_contact" name="contact" class="form-control" placeholder="<?php _e('PHONE NUMBER', 'holpack'); ?>" />
            <small class="danger custom-danger d-none error-phone"></small>
        </div>
        <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input type="text" id="modal_company_contact" name="company" class="form-control" placeholder="<?php _e('COMPANY', 'holpack'); ?>" />
            <small class="danger custom-danger d-none error-company"></small>
        </div>
        <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input type="text" id="modal_zipcode_contact" name="zipcode" class="form-control" placeholder="<?php _e('ZIP OR POSTAL CODE', 'holpack'); ?>" />
            <small class="danger custom-danger d-none error-zipcode"></small>
        </div>
        <div class="contact-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <input type="text" autocomplete="false" id="modal_country_contact" name="country" class="form-control" placeholder="<?php _e('COUNTRY', 'holpack'); ?>" />
            <small class="danger custom-danger d-none error-country"></small>
        </div>
        <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <textarea name="message" id="modal_message_contact" cols="30" rows="3" class="form-control" placeholder="<?php _e('MESSAGE', 'holpack'); ?>"></textarea>
            <small class="danger custom-danger d-none error-message"></small>
        </div>
        <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="g-recaptcha" id="recaptchaModal" data-callback="recaptchaModalCallback"></div>
            <small id="errorModalRecaptcha" class="danger custom-danger d-none error-recaptcha"><?php _e('Error: You must validate this recatpcha', 'taurusfood'); ?></small>
        </div>
        <div class="contact-form-submit col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <button id="modalSubmitBtn" class="btn btn-lg btn-submit"><?php _e('Submit', 'holpack'); ?></button>
        </div>
        <div class="modal-form-response contact-form-response col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="modal-loader-css loader-css d-none"></div>
        </div>
    </div>
</form>