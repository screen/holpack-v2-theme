<div class="main-banner-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <?php $bg_banner_id = get_post_meta(get_the_ID(), 'hlp_banner_bg_id', true); ?>
    <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'banner_img', false); ?>
    <img itemprop="banner" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" data-aos="fadeIn" data-aos-delay="150" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
    <div class="main-banner-wrapper">
        <h1><?php the_title(); ?></h1>
    </div>
</div>
