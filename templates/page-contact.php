<?php
/**
* Template Name: Page Template  - Contact Us
*
* @package holpack
* @subpackage holpack-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php get_template_part('templates/templates-banner-container'); ?>
        <div class="contact-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="contact-main-form col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-1 order-sm-12 order-12">
                        <?php the_content(); ?>
                        <?php echo get_template_part('templates/templates-contact-form'); ?>
                    </div>
                    <div class="contact-main-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-12 order-lg-12 order-md-12 order-sm-1 order-1">
                        <div class="contact-main-content-wrapper">
                            <h2><?php _e('Contact Info', 'holpack');?></h2>
                            <div class="contact-main-embed">
                                <?php $contact_embed = get_post_meta(get_the_ID(), 'hlp_contact_embed', true); ?>
                                <div class="embed-responsive embed-responsive-16by9">
                                    <?php echo $contact_embed; ?>
                                </div>
                            </div>
                            <div class="contact-main-item">
                                <i class="fa fa-map-marker"></i>
                                <p><?php echo get_post_meta(get_the_ID(), 'hlp_contact_dir', true); ?></p>
                            </div>
                            <div class="contact-main-item">
                                <i class="fa fa-phone"></i>
                                <p><a href="tel:<?php echo preg_replace("/[^a-zA-Z0-9]+/", "", get_post_meta(get_the_ID(), 'hlp_contact_telf', true)); ?>"><?php echo get_post_meta(get_the_ID(), 'hlp_contact_telf', true); ?></a></p>
                            </div>
                            <div class="contact-main-item">
                                <i class="fa fa-envelope"></i>
                                <p><a href="mailto:<?php echo get_post_meta(get_the_ID(), 'hlp_contact_email', true); ?>"><?php echo get_post_meta(get_the_ID(), 'hlp_contact_email', true); ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
