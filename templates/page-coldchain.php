<?php
/**
* Template Name: Page Template  - ColdChain Solutions
*
* @package holpack
* @subpackage holpack-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="coldchain-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="coldchain-main-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h1><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </section>
        <?php $category_selected = get_post_meta(get_the_ID(), 'hlp_products_category', true); ?>
        <?php $args = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'tax_query' => array(array('taxonomy' => 'product_cat', 'terms' => array($category_selected), 'field' => 'term_id'))); ?>
        <?php $arr_products = new WP_Query($args); ?>
        <?php if ($arr_products->have_posts()) : ?>
        <section class="coldchain-products-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <?php while ($arr_products->have_posts()) : $arr_products->the_post(); ?>
                    <div class="coldchain-product-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <picture>
                            <a href="<?php the_permalink(); ?>" title="<?php _e('See More', 'holpack'); ?>">
                                <?php the_post_thumbnail('woocommerce_thumbnail', array('class' => 'img-fluid'));?>
                            </a>
                        </picture>
                        <a href="<?php the_permalink(); ?>" title="<?php _e('See More', 'holpack'); ?>">
                            <h2><?php the_title(); ?></h2>
                        </a>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div>
</main>
<?php get_footer(); ?>
