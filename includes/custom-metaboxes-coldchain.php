<?php

/* SLIDER REVOLUTION */
$array_sliders = array();

$args = array('taxonomy' => 'product_cat', 'hide_empty' => false);
$arr_cats = get_terms($args);
if (!empty($arr_cats)) {
    foreach ($arr_cats as $item) {
        $array_sliders[$item->term_id] = $item->name;
    }
}
/* SLIDER REVOLUTION */


/*------------ 2.- MEDIA SECCION ------------*/
$cmb_coldchain_page = new_cmb2_box( array(
    'id'            => $prefix . 'coldchain_media',
    'title'         => esc_html__( 'Home: Media Section', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-coldchain.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_coldchain_page->add_field( array(
    'id'         => $prefix . 'products_category',
    'name'       => esc_html__( 'Products Category', 'gespetfood' ),
    'desc'       => esc_html__( 'Seleccione el Slider a usar en el Home', 'gespetfood' ),
    'type'             => 'select',
    'show_option_none' => true,
    'default'          => '',
    'options'          => $array_sliders
) );
