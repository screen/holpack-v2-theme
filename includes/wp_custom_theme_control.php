<?php
/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action('customize_register', 'holpack_customize_register');

function holpack_customize_register($wp_customize)
{
    /* HEADER */
    $wp_customize->add_section('hlp_header_settings', array(
        'title'    => __('Header', 'holpack'),
        'description' => __('Options for Header Elements', 'holpack'),
        'priority' => 30
    ));

    $wp_customize->add_setting('hlp_header_settings[phone_number]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('phone_number', array(
        'type' => 'text',
        'label'    => __('Phone Number', 'holpack'),
        'description' => __('Add phone number formatted for link EG: tel:+1234567874', 'holpack'),
        'section'  => 'hlp_header_settings',
        'settings' => 'hlp_header_settings[phone_number]'
    ));

    $wp_customize->add_setting('hlp_header_settings[header_phone_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('header_phone_text', array(
        'type' => 'text',
        'label'    => __('Phone Number Text', 'holpack'),
        'description' => __('Add Phone Text for Header Button Link', 'holpack'),
        'section'  => 'hlp_header_settings',
        'settings' => 'hlp_header_settings[header_phone_text]'
    ));

    $wp_customize->add_setting('hlp_header_settings[header_button_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('header_button_text', array(
        'type' => 'text',
        'label'    => __('Header Text', 'holpack'),
        'description' => __('Add descriptive text above the phone number', 'holpack'),
        'section'  => 'hlp_header_settings',
        'settings' => 'hlp_header_settings[header_button_text]'
    ));

    /* SOCIAL */
    $wp_customize->add_section('hlp_social_settings', array(
        'title'    => __('Redes Sociales', 'holpack'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'holpack'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('hlp_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'holpack_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('facebook', array(
        'type' => 'url',
        'section' => 'hlp_social_settings',
        'settings' => 'hlp_social_settings[facebook]',
        'label' => __('Facebook', 'holpack'),
    ));

    $wp_customize->add_setting('hlp_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'holpack_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('twitter', array(
        'type' => 'url',
        'section' => 'hlp_social_settings',
        'settings' => 'hlp_social_settings[twitter]',
        'label' => __('Twitter', 'holpack'),
    ));

    $wp_customize->add_setting('hlp_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'holpack_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('instagram', array(
        'type' => 'url',
        'section' => 'hlp_social_settings',
        'settings' => 'hlp_social_settings[instagram]',
        'label' => __('Instagram', 'holpack'),
    ));

    $wp_customize->add_setting('hlp_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'holpack_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('linkedin', array(
        'type' => 'url',
        'section' => 'hlp_social_settings',
        'settings' => 'hlp_social_settings[linkedin]',
        'label' => __('LinkedIn', 'holpack'),
    ));

    $wp_customize->add_setting('hlp_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'holpack_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('youtube', array(
        'type' => 'url',
        'section' => 'hlp_social_settings',
        'settings' => 'hlp_social_settings[youtube]',
        'label' => __('YouTube', 'holpack'),
    ));

    $wp_customize->add_setting('hlp_social_settings[yelp]', array(
        'default'           => '',
        'sanitize_callback' => 'holpack_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('yelp', array(
        'type' => 'url',
        'section' => 'hlp_social_settings',
        'settings' => 'hlp_social_settings[yelp]',
        'label' => __('Yelp', 'holpack'),
    ));


    $wp_customize->add_section('hlp_cookie_settings', array(
        'title'    => __('Cookies', 'holpack'),
        'description' => __('Opciones de Cookies', 'holpack'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('hlp_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'holpack'),
        'description' => __('Texto del Cookie consent.'),
        'section'  => 'hlp_cookie_settings',
        'settings' => 'hlp_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('hlp_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'hlp_cookie_settings',
        'settings' => 'hlp_cookie_settings[cookie_link]',
        'label' => __('Link de Cookies', 'holpack'),
    ));

    /* GOOGLE RECAPTCHA */
    $wp_customize->add_section('hlp_google_settings', array(
        'title'    => __('Google', 'holpack'),
        'description' => __('Google Recaptcha Data', 'holpack'),
        'priority' => 180,
    ));

    $wp_customize->add_setting('hlp_google_settings[sitekey]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('sitekey', array(
        'type' => 'text',
        'label'    => __('SiteKey', 'holpack'),
        'description' => __('Enter Recaptcha API SiteKey.'),
        'section'  => 'hlp_google_settings',
        'settings' => 'hlp_google_settings[sitekey]'
    ));

    $wp_customize->add_setting('hlp_google_settings[secretkey]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('secretkey', array(
        'type' => 'text',
        'label'    => __('SecretKey', 'holpack'),
        'description' => __('Enter Recaptcha API SecretKey.'),
        'section'  => 'hlp_google_settings',
        'settings' => 'hlp_google_settings[secretkey]'
    ));

    /* MODAL */
    $wp_customize->add_section('hlp_modal_settings', array(
        'title'    => __('Modal', 'holpack'),
        'description' => __('Options for Modal Elements', 'holpack'),
        'priority' => 181
    ));

    $wp_customize->add_setting('hlp_modal_settings[modal_activate]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('modal_activate', array(
        'type' => 'checkbox',
        'label'    => __('Activate Modal Elements', 'holpack'),
        'description' => __('Click here if you want to Activate the Modal Elements'),
        'section'  => 'hlp_modal_settings',
        'settings' => 'hlp_modal_settings[modal_activate]'
    ));

    $wp_customize->add_setting('hlp_modal_settings[button_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('button_text', array(
        'type' => 'text',
        'label'    => __('Button Text', 'holpack'),
        'description' => __('Enter Button Text for Modal.'),
        'section'  => 'hlp_modal_settings',
        'settings' => 'hlp_modal_settings[button_text]'
    ));

    $wp_customize->add_setting('hlp_modal_settings[modal_title]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('modal_title', array(
        'type' => 'text',
        'label'    => __('Modal Title Text', 'holpack'),
        'description' => __('Enter Title Text for Modal.'),
        'section'  => 'hlp_modal_settings',
        'settings' => 'hlp_modal_settings[modal_title]'
    ));

    $wp_customize->add_setting('hlp_modal_settings[form_text]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('form_text', array(
        'type' => 'textarea',
        'label'    => __('Form Text', 'holpack'),
        'description' => __('Enter Form Text for Modal.'),
        'section'  => 'hlp_modal_settings',
        'settings' => 'hlp_modal_settings[form_text]'
    ));

    $wp_customize->add_setting('diwp_logo', array(
        'default' => get_theme_file_uri('assets/image/logo.jpg'), // Add Default Image URL
        'sanitize_callback' => 'esc_url_raw'
    ));

    $wp_customize->add_setting('hlp_modal_settings[modal_image]', array(
        'default'           => '',
        'sanitize_callback' => 'esc_url_raw',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));
 
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'modal_image', array(
        'label'    => __('Modal Image', 'holpack'),
        'description' => __('Enter Imagen for Modal.'),
        'priority' => 20,
        'section' => 'hlp_modal_settings',
        'settings' => 'hlp_modal_settings[modal_image]',
        'button_labels' => array(// All These labels are optional
                    'select' => 'Select Image',
                    'remove' => 'Remove Image',
                    'change' => 'Change Image',
                    )
    )));
}

function holpack_sanitize_url($url)
{
    return esc_url_raw($url);
}
