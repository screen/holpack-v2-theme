<?php

function holpack_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'holpack' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'holpack' ),
		'menu_name'             => __( 'Clientes', 'holpack' ),
		'name_admin_bar'        => __( 'Clientes', 'holpack' ),
		'archives'              => __( 'Archivo de Clientes', 'holpack' ),
		'attributes'            => __( 'Atributos de Cliente', 'holpack' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'holpack' ),
		'all_items'             => __( 'Todos los Clientes', 'holpack' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'holpack' ),
		'add_new'               => __( 'Agregar Nuevo', 'holpack' ),
		'new_item'              => __( 'Nuevo Cliente', 'holpack' ),
		'edit_item'             => __( 'Editar Cliente', 'holpack' ),
		'update_item'           => __( 'Actualizar Cliente', 'holpack' ),
		'view_item'             => __( 'Ver Cliente', 'holpack' ),
		'view_items'            => __( 'Ver Clientes', 'holpack' ),
		'search_items'          => __( 'Buscar Cliente', 'holpack' ),
		'not_found'             => __( 'No hay resultados', 'holpack' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'holpack' ),
		'featured_image'        => __( 'Imagen del Cliente', 'holpack' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'holpack' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'holpack' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'holpack' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'holpack' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'holpack' ),
		'items_list'            => __( 'Listado de Clientes', 'holpack' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'holpack' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'holpack' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'holpack' ),
		'description'           => __( 'Portafolio de Clientes', 'holpack' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'holpack_custom_post_type', 0 );