<?php
/* WOOCOMMERCE CUSTOM COMMANDS */

/* WOOCOMMERCE - DECLARE THEME SUPPORT - BEGIN */
add_action( 'after_setup_theme', 'holpack_woocommerce_support' );
function holpack_woocommerce_support() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}
/* WOOCOMMERCE - DECLARE THEME SUPPORT - END */

/* WOOCOMMERCE - CUSTOM WRAPPER - BEGIN */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'holpack_woocommerce_custom_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'holpack_woocommerce_custom_wrapper_end', 10);

function holpack_woocommerce_custom_wrapper_start() {
    echo '<section id="main" class="container"><div class="row"><div class="woocustom-main-container col-12">';
}

function holpack_woocommerce_custom_wrapper_end() {
    echo '</div></div></section>';
}
/* WOOCOMMERCE - CUSTOM WRAPPER - END */

/* WOOCOMMERCE - ARCHIVE PRODUCT - START */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
add_action('woocommerce_before_main_content', 'custom_woocommerce_backlink', 10);
add_action('woocommerce_archive_description', 'custom_woocommerce_nav_categories', 20);

function custom_woocommerce_backlink() {
    ob_start();
    if (is_single()) {
        $terms = get_the_terms(get_the_ID(), 'product_cat');
?>
<a href="<?php echo wc_get_page_permalink( 'shop' ); ?>#<?php echo $terms[0]->slug; ?>" class="custom-woocommerce-backlink"><span class="chevron">&#60;</span> <span class="backlink-text"><?php _e('Go back to products', 'holpack'); ?></span></a>
<?php
    }
    $content = ob_get_clean();
    echo $content;
}

function custom_woocommerce_nav_categories() {
    ob_start();
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="custom-woocommerce-nav-container col-xl-11 col-lg-11 col-md-11 col-sm-12 col-12">
            <ul class="nav nav-tabs  justify-content-center" id="productTab" role="tablist">
                <?php $args = array('taxonomy' => 'product_cat', 'hide_empty' => true); ?>
                <?php $array_terms = get_terms($args); ?>
                <?php $i = 1; ?>
                <?php if (!empty($array_terms)) : ?>
                <?php foreach ($array_terms as $item) { ?>
                <?php $class = ($i == 1) ? 'active' : ''; ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo $class; ?>" id="<?php echo $item->slug; ?>-tab" data-toggle="tab" href="#<?php echo $item->slug; ?>" role="tab" aria-controls="<?php echo $item->slug; ?>" aria-selected="false"><?php echo $item->name; ?></a>
                </li>
                <?php $i++; } ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
<?php
    $content = ob_get_clean();
    echo $content;
}
/* WOOCOMMERCE - ARCHIVE PRODUCT - END */

/* WOOCOMMERCE - CUSTOM CONTENT PRODUCT - SHOP - START */
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
/* WOOCOMMERCE - CUSTOM CONTENT PRODUCT - SHOP - END */


/* WOOCOMMERCE - SINGLE PRODUCT - START */

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
add_action('woocommerce_single_product_summary', 'custom_woocommerce_breadcrumb', 2);
add_action('woocommerce_single_product_summary', 'custom_woocommerce_product_data_tabs', 20);
add_action('woocommerce_single_product_summary', 'custom_woocommerce_template_single_sharing', 50);
add_action('woocommerce_after_single_product_summary', 'custom_woocommerce_request_quote', 20);


function custom_woocommerce_breadcrumb() {
    ob_start();
?>
<ul class="custom-woocommerce-breadcrumb">
    <li><a href="<?php echo home_url('/'); ?>"><?php _e('Home', 'holpack'); ?></a></li>
    <li><a href="<?php echo wc_get_page_permalink( 'shop' ); ?>"><?php _e('Products', 'holpack'); ?></a></li>
</ul>
<?php
    $content = ob_get_clean();
    echo $content;
}

function custom_woocommerce_product_data_tabs() {
    ob_start();
?>
<ul class="nav nav-tabs custom-nav justify-content-center" id="singleTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="desc-tab" data-toggle="tab" href="#desc" role="tab" aria-controls="desc" aria-selected="true"><?php _e('Description', 'holpack'); ?></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="features-tab" data-toggle="tab" href="#features" role="tab" aria-controls="features" aria-selected="false"><?php _e('Features', 'holpack'); ?></a>
    </li>
    <?php $active_third_tab = get_post_meta(get_the_ID(), 'hlp_product_specs_activate', true); ?>
    <?php if ($active_third_tab != 'on') { ?>
    <li class="nav-item">
        <?php $name = get_post_meta(get_the_ID(), 'hlp_product_specs_name', true); ?>
        <a class="nav-link" id="third-tab" data-toggle="tab" href="#third" role="tab" aria-controls="third" aria-selected="false"><?php echo $name; ?></a>
    </li>
    <?php } ?>
</ul>
<div class="tab-content custom-tab-content" id="singleTabContent">
    <div class="tab-pane fade show active" id="desc" role="tabpanel" aria-labelledby="desc-tab">
        <?php the_content(); ?>
    </div>
    <div class="tab-pane fade" id="features" role="tabpanel" aria-labelledby="features-tab">
        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hlp_product_features', true)); ?>
    </div>
    <?php if ($active_third_tab != 'on') { ?>
    <div class="tab-pane tab-pane-specs fade" id="third" role="tabpanel" aria-labelledby="third-tab">
        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'hlp_product_specs', true)); ?>
    </div>
    <?php } ?>
</div>
<?php
    $content = ob_get_clean();
    echo $content;
}

function custom_woocommerce_template_single_sharing() {
    if (is_single()) {
        ob_start();
?>
<div class="custom-social-share">
    <?php $custom_text = ''; ?>
    <span><?php _e('SHARE', 'holpack'); ?>:</span>
    <div class="custom-social-share-content">
        <a href="https://pinterest.com/pin/create/bookmarklet/?media=<?php echo get_the_post_thumbnail_url(); ?>&url=<?php echo get_permalink(); ?>&description=<?php echo $custom_text . ' ' . get_the_title(); ?>" target="_blank"><i class="fa fa-pinterest"></i></a>
        <a href="https://twitter.com/share?url=<?php echo get_permalink(); ?>&text=<?php echo $custom_text . ' ' . get_the_title(); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
        <a href="https://api.whatsapp.com/send?text=<?php echo $custom_text . ' ' . get_the_title(); ?> <?php echo get_permalink(); ?>" target="_blank"><i class="fa fa-whatsapp"></i></a>

    </div>
</div>
<?php
        $content = ob_get_clean();
        echo $content;
    }
}

function custom_woocommerce_request_quote() {
    ob_start();
?>
<div class="custom-request-quote-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="container">
        <div class="row justify-content-center">
            <div class="custom-request-quote-content  col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                <?php $shop_id = get_option( 'woocommerce_shop_page_id' ); ?>
                <?php echo apply_filters('the_content', get_post_meta($shop_id, 'hlp_shop_request_quote', true)); ?>
                <?php echo get_template_part('templates/templates-contact-form'); ?>
            </div>
        </div>
    </div>
</div>
<?php
    $content = ob_get_clean();
    echo $content;
}

/* WOOCOMMERCE - SINGLE PRODUCT - END */
