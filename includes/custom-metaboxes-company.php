<?php
/*------------ 2.- MEDIA SECCION ------------*/
$cmb_company_media = new_cmb2_box( array(
    'id'            => $prefix . 'company_media',
    'title'         => esc_html__( 'Home: Media Section', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-company.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_company_media->add_field( array(
    'id'   => $prefix . 'media_image',
    'name'      => esc_html__( 'Imagen del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Cargar una imagen para esta sección', 'holpack' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'holpack' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_company_media->add_field( array(
    'id'   => $prefix . 'media_desc',
    'name'      => esc_html__( 'Descripción del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'holpack' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );

/* HOME: SOLUTIONS SECTION */
$cmb_home_solutions = new_cmb2_box( array(
    'id'            => $prefix . 'company_benefits',
    'title'         => esc_html__( 'Company: Benefits Section', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-company.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$group_field_id = $cmb_home_solutions->add_field( array(
    'id'          => $prefix . 'benefits_items_group',
    'type'        => 'group',
    'description' => __( 'Beneficios dentro del Home - Ubicado justo despues del banner principal', 'xsl' ),
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'xsl' ),
        'add_button'        => __( 'Add Other Item', 'xsl' ),
        'remove_button'     => __( 'Remover Beneficio', 'xsl' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este item?', 'xsl' )
    )
) );

$cmb_home_solutions->add_group_field( $group_field_id, array(
    'id'   => 'item_bg',
    'name'      => esc_html__( 'Logo Image', 'holpack' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'holpack' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'holpack' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_solutions->add_group_field( $group_field_id, array(
    'id'   => 'title',
    'name'      => esc_html__( 'Titulo del Item', 'xsl' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Item', 'xsl' ),
    'type' => 'text'
) );

$cmb_home_solutions->add_group_field( $group_field_id, array(
    'id'   => 'description',
    'name'      => esc_html__( 'Descripción del Item', 'xsl' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Item', 'xsl' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );

/* HOME: SOLUTIONS SECTION */
$cmb_home_testimonials = new_cmb2_box( array(
    'id'            => $prefix . 'company_testimonials',
    'title'         => esc_html__( 'Company: Testimonial Section', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-company.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_testimonials->add_field( array(
    'id'   => $prefix . 'testimonial_bg',
    'name'      => esc_html__( 'Section BG', 'holpack' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'holpack' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'holpack' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );


$group_field_id = $cmb_home_testimonials->add_field( array(
    'id'          => $prefix . 'testimonials_items_group',
    'type'        => 'group',
    'description' => __( 'Beneficios dentro del Home - Ubicado justo despues del banner principal', 'xsl' ),
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'xsl' ),
        'add_button'        => __( 'Add Other Item', 'xsl' ),
        'remove_button'     => __( 'Remover Beneficio', 'xsl' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este item?', 'xsl' )
    )
) );

$cmb_home_testimonials->add_group_field( $group_field_id, array(
    'id'   => 'testimonial',
    'name'      => esc_html__( 'Descripción del Item', 'xsl' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Item', 'xsl' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );

$cmb_home_testimonials->add_group_field( $group_field_id, array(
    'id'   => 'autor',
    'name'      => esc_html__( 'Titulo del Item', 'xsl' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Item', 'xsl' ),
    'type' => 'text'
) );


$cmb_company_mission = new_cmb2_box( array(
    'id'            => $prefix . 'company_mission',
    'title'         => esc_html__( 'Home: Mission Section', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-company.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_company_mission->add_field( array(
    'id'   => $prefix . 'mission_desc',
    'name'      => esc_html__( 'Mission Content', 'holpack' ),
    'desc'      => esc_html__( 'Ingresa una descripción para el Hero', 'holpack' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );

/* HOME: SOLUTIONS SECTION */
$cmb_company_values = new_cmb2_box( array(
    'id'            => $prefix . 'company_values',
    'title'         => esc_html__( 'Company: Values Section', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-company.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_company_values->add_field( array(
    'id'   => $prefix . 'values_title',
    'name'      => esc_html__( 'Mission Content', 'holpack' ),
    'desc'      => esc_html__( 'Ingresa una descripción para el Hero', 'holpack' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$group_field_id = $cmb_company_values->add_field( array(
    'id'          => $prefix . 'values_items_group',
    'type'        => 'group',
    'description' => __( 'Beneficios dentro del Home - Ubicado justo despues del banner principal', 'xsl' ),
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'xsl' ),
        'add_button'        => __( 'Add Other Item', 'xsl' ),
        'remove_button'     => __( 'Remover Beneficio', 'xsl' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este item?', 'xsl' )
    )
) );

$cmb_company_values->add_group_field( $group_field_id, array(
    'id'   => 'item_bg',
    'name'      => esc_html__( 'Logo Image', 'holpack' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'holpack' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'holpack' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_company_values->add_group_field( $group_field_id, array(
    'id'   => 'title',
    'name'      => esc_html__( 'Titulo del Item', 'xsl' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Item', 'xsl' ),
    'type' => 'text'
) );

$cmb_company_values->add_group_field( $group_field_id, array(
    'id'   => 'description',
    'name'      => esc_html__( 'Descripción del Item', 'xsl' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Item', 'xsl' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );
