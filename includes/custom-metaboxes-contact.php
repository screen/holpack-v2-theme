<?php

/* CONTACT EMBED ELEMENT */
$cmb_contact_embed = new_cmb2_box( array(
    'id'            => $prefix . 'contact_embed_metabox',
    'title'         => esc_html__( 'Contact: Main Info Section', 'xsl' ),
    'object_types'  => array( 'page' ), // Post type
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-contact.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true, // Show field names on the left
    'closed'     => false, // true to keep the metabox closed by default
) );

$cmb_contact_embed->add_field( array(
    'id'   => $prefix . 'contact_embed',
    'name'      => esc_html__( 'Código Embed del Mapa', 'xsl' ),
    'desc'      => esc_html__( 'Inserte el iframe que generara el Mapa en el sitio', 'xsl' ),
    'type'    => 'textarea_code'
) );

$cmb_contact_embed->add_field( array(
    'id'   => $prefix . 'contact_dir',
    'name'      => esc_html__( 'Direction', 'xsl' ),
    'desc'      => esc_html__( 'Inserte el iframe que generara el Mapa en el sitio', 'xsl' ),
    'type'    => 'textarea_small'
) );

$cmb_contact_embed->add_field( array(
    'id'   => $prefix . 'contact_telf',
    'name'      => esc_html__( 'Phone', 'xsl' ),
    'desc'      => esc_html__( 'Inserte el iframe que generara el Mapa en el sitio', 'xsl' ),
    'type'    => 'text'
) );

$cmb_contact_embed->add_field( array(
    'id'   => $prefix . 'contact_email',
    'name'      => esc_html__( 'Email', 'xsl' ),
    'desc'      => esc_html__( 'Inserte el iframe que generara el Mapa en el sitio', 'xsl' ),
    'type'    => 'text'
) );
