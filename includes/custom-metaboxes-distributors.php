<?php

/* HOME: SOLUTIONS SECTION */
$cmb_distributor_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'distributors_metabox',
    'title'         => esc_html__( 'Distributors: Form Section', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-distributor.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_distributor_metabox->add_field( array(
    'id'   => $prefix . 'distributor_form_desc',
    'name'      => esc_html__( 'Form Description', 'xsl' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Item', 'xsl' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );
