<?php
$cmb_main_banner = new_cmb2_box( array(
    'id'            => $prefix . 'main_banner',
    'title'         => esc_html__( 'Main Banner', 'holpack' ),
    'object_types'  => array( 'page' ),
    'context'    => 'side',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_main_banner->add_field( array(
    'id'   => $prefix . 'banner_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'holpack' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'holpack' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );
