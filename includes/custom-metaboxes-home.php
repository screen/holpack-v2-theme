<?php
/*------------ 1.- HOME ------------*/
$cmb_home_hero = new_cmb2_box( array(
    'id'            => $prefix . 'home_hero',
    'title'         => esc_html__( 'Home: Hero Principal', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'hero_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'holpack' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'holpack' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );


$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'hero_image',
    'name'      => esc_html__( 'Imagen del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Cargar una imagen para esta sección', 'holpack' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'holpack' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'hero_title',
    'name'      => esc_html__( 'Título del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'holpack' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );

$group_field_id = $cmb_home_hero->add_field( array(
    'id'          => $prefix . 'hero_items_group',
    'type'        => 'group',
    'description' => __( 'Beneficios dentro del Home - Ubicado justo despues del banner principal', 'xsl' ),
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'xsl' ),
        'add_button'        => __( 'Add Other Item', 'xsl' ),
        'remove_button'     => __( 'Remover Beneficio', 'xsl' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este item?', 'xsl' )
    )
) );

$cmb_home_hero->add_group_field( $group_field_id, array(
    'id'   => 'description',
    'name'      => esc_html__( 'Descripción del Item', 'xsl' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Item', 'xsl' ),
    'type' => 'text'
) );

$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'hero_subtitle',
    'name'      => esc_html__( 'Subtítulo del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'holpack' ),
    'type' => 'text'
) );

$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'hero_button_text',
    'name'      => esc_html__( 'Texto del Boton', 'holpack' ),
    'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'holpack' ),
    'type' => 'text'
) );

$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'hero_button_link',
    'name'      => esc_html__( 'URL del Boton', 'holpack' ),
    'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'holpack' ),
    'type' => 'text_url'
) );

/*------------ 2.- MEDIA SECCION ------------*/
$cmb_home_media = new_cmb2_box( array(
    'id'            => $prefix . 'home_media',
    'title'         => esc_html__( 'Home: Media Section', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_media->add_field( array(
    'id'   => $prefix . 'media_image',
    'name'      => esc_html__( 'Imagen del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Cargar una imagen para esta sección', 'holpack' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'holpack' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_media->add_field( array(
    'id'   => $prefix . 'media_title',
    'name'      => esc_html__( 'Título del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'holpack' ),
    'type' => 'text'
) );

$cmb_home_media->add_field( array(
    'id'   => $prefix . 'media_desc',
    'name'      => esc_html__( 'Descripción del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'holpack' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );

/* HOME: SOLUTIONS SECTION */
$cmb_home_solutions = new_cmb2_box( array(
    'id'            => $prefix . 'home_solutions',
    'title'         => esc_html__( 'Home: Solutions Section', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_solutions->add_field( array(
    'id'   => $prefix . 'solutions_title',
    'name'      => esc_html__( 'Título del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Ingresa un Título Descriptivo para el Hero', 'holpack' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );

$group_field_id = $cmb_home_solutions->add_field( array(
    'id'          => $prefix . 'solutions_items_group',
    'type'        => 'group',
    'description' => __( 'Beneficios dentro del Home - Ubicado justo despues del banner principal', 'xsl' ),
    'options'     => array(
        'group_title'       => __( 'Item {#}', 'xsl' ),
        'add_button'        => __( 'Add Other Item', 'xsl' ),
        'remove_button'     => __( 'Remover Beneficio', 'xsl' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este item?', 'xsl' )
    )
) );

$cmb_home_solutions->add_group_field( $group_field_id, array(
    'id'   => 'item_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'holpack' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'holpack' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_solutions->add_group_field( $group_field_id, array(
    'id'   => 'title',
    'name'      => esc_html__( 'Descripción del Item', 'xsl' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Item', 'xsl' ),
    'type' => 'text'
) );

$cmb_home_solutions->add_group_field( $group_field_id, array(
    'id'   => 'description',
    'name'      => esc_html__( 'Descripción del Item', 'xsl' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Item', 'xsl' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );

/*------------ 3.- CONTACT SECCION ------------*/
$cmb_home_contact = new_cmb2_box( array(
    'id'            => $prefix . 'home_contact',
    'title'         => esc_html__( 'Home: Contact Section', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_contact->add_field( array(
    'id'   => $prefix . 'contact_image',
    'name'      => esc_html__( 'Imagen del Hero', 'holpack' ),
    'desc'      => esc_html__( 'Cargar una imagen para esta sección', 'holpack' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'holpack' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_contact->add_field( array(
    'id'   => $prefix . 'contact_desc',
    'name'      => esc_html__( 'Descripción del Item', 'xsl' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva al Item', 'xsl' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );
