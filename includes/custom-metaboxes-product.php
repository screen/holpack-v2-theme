<?php

/* PRODUCT: SHOP */
$cmb_product_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'shop_metabox',
    'title'         => esc_html__( 'Shop: Extra Information', 'holpack' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'slug', 'value' => array( 'shop', 'tienda', 'products', 'productos' ) ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_product_metabox->add_field( array(
    'id'   => $prefix . 'shop_request_quote',
    'name'      => esc_html__( 'Request a Quote Description', 'xsl' ),
    'desc'      => esc_html__( 'Enter a description for the request a quote form', 'holpack' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );

/* PRODUCT: SINGLE PRODUCT */
$cmb_product_metabox = new_cmb2_box( array(
    'id'            => $prefix . 'product_metabox',
    'title'         => esc_html__( 'Product: Extra Information', 'holpack' ),
    'object_types'  => array( 'product' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_product_metabox->add_field( array(
    'id'   => $prefix . 'product_features',
    'name'      => esc_html__( 'Product Features', 'xsl' ),
    'desc'      => esc_html__( 'Enter the Product Features in the Tab Section', 'holpack' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );

$cmb_product_metabox->add_field( array(
    'id'   => $prefix . 'product_specs_activate',
    'name'      => esc_html__( 'Dectivate the third Tab', 'xsl' ),
    'desc'      => esc_html__( 'click here if you want to deactivate this third tab', 'holpack' ),
    'type' => 'checkbox'
) );

$cmb_product_metabox->add_field( array(
    'id'   => $prefix . 'product_specs_name',
    'name'      => esc_html__( 'Product Specs Name title', 'xsl' ),
    'desc'      => esc_html__( 'Enter the title for this Tab Section', 'holpack' ),
    'type' => 'text'
) );

$cmb_product_metabox->add_field( array(
    'id'   => $prefix . 'product_specs',
    'name'      => esc_html__( 'Product Specs', 'xsl' ),
    'desc'      => esc_html__( 'Enter the Product Specs in the Tab Section', 'holpack' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 4),
        'teeny' => false
    )
) );
